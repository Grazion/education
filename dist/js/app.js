/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/***/ (() => {

document.addEventListener("DOMContentLoaded", function () {
  //прелоудер
  window.onload = function () {
    window.setTimeout(function () {
      document.querySelector(".loader__wrap").style.display = "none";
      document.body.classList.remove('overflow');
    }, 500);
  }; //бургер меню


  $('.header__burger, .overlay').click(function () {
    $('.header').toggleClass('show');
    $('body').toggleClass('overflow');
  });
  $("#nav").on("click", ".nav__link", function (event) {
    $('.header').removeClass('show');
    $('body').removeClass('overflow');
  }); //плавный скролл

  $("body").on("click", "a[href^=\"#\"]", function (event) {
    event.preventDefault();
    var id = $(this).attr('href'),
        top = $(id).offset().top;
    $('body,html').animate({
      scrollTop: top
    }, 1500);
  }); //слайдеры

  var projects__slider1 = new Swiper(".projects__slider", {
    on: {
      slideChange: function slideChange() {
        var blockBack = document.querySelector(".projects__container"); // const imgIndex = document.querySelector(".projects__img_slide" + this.realIndex);
        // console.log(imgIndex)

        if (this.realIndex === 0) {
          blockBack.style.background = "linear-gradient(to right, #244EA0 0%, #244EA0 50%, #189E3D 0%, #189E3D 100%)";
          document.querySelector(".projects__img_slide1").style.opacity = "1";
        } else if (this.realIndex === 1) {
          blockBack.style.background = "linear-gradient(to right, #189E3D 0%, #189E3D 50%, #5CBAFA 0%, #5CBAFA 100%)";
          document.querySelector(".projects__img_slide1").style.opacity = "0";
        } else if (this.realIndex === 2) {
          blockBack.style.background = "linear-gradient(to right, #5CBAFA 0%, #5CBAFA 50%, #8F56D7 0%, #8F56D7 100%)";
        } else if (this.realIndex === 3) {
          blockBack.style.background = "linear-gradient(to right, #8F56D7 0%, #8F56D7 50%, #099FF6 0%, #099FF6 100%)";
        } else if (this.realIndex === 4) {
          blockBack.style.background = "linear-gradient(to right, #099FF6 0%, #099FF6 50%, #244EA0 0%, #244EA0 100%)";
        }
      }
    },
    slidesPerView: 1,
    spaceBetween: 0,
    allowTouchMove: false,
    speed: 500
  });
  var projects__slider2 = new Swiper(".projects-text__slider", {
    slidesPerView: 1,
    spaceBetween: 0,
    autoHeight: true,
    loop: true,
    speed: 500,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    on: {
      init: function init() {
        var _this = this;

        this.el.addEventListener('mouseenter', function () {
          _this.autoplay.stop();
        });
        this.el.addEventListener('mouseleave', function () {
          _this.autoplay.start();
        });
      }
    },
    navigation: {
      nextEl: '.projects-text__btn_next',
      prevEl: '.projects-text__btn_prev'
    }
  });

  var swipeAllSliders = function swipeAllSliders(index) {
    projects__slider1.slideToLoop(index);
    projects__slider2.slideToLoop(index);
  };

  projects__slider1.on('slideChange', function () {
    return swipeAllSliders(projects__slider1.realIndex);
  });
  projects__slider2.on('slideChange', function () {
    return swipeAllSliders(projects__slider2.realIndex);
  }); //табы

  (function ($) {
    $('.tab .tab__tabs .tab__tab').click(function (g) {
      var tab = $(this).closest('.tab'),
          index = $(this).closest('.tab__tab').index();
      tab.find('.tab__tabs > .tab__tab').removeClass('active');
      $(this).closest('.tab__tab').addClass('active');
      tab.find('.tab__content').find('.tab__block').not('.tab__block:eq(' + index + ')').slideUp();
      tab.find('.tab__content').find('.tab__block:eq(' + index + ')').slideDown();
      g.preventDefault();
    });
  })(jQuery); //телефон


  window.addEventListener("DOMContentLoaded", function () {
    [].forEach.call(document.querySelectorAll('.tel'), function (input) {
      var keyCode;

      function mask(event) {
        event.keyCode && (keyCode = event.keyCode);
        var pos = this.selectionStart;
        if (pos < 3) event.preventDefault();
        var matrix = "+7 (___) ___-__-__",
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, ""),
            new_value = matrix.replace(/[_\d]/g, function (a) {
          return i < val.length ? val.charAt(i++) || def.charAt(i) : a;
        });
        i = new_value.indexOf("_");

        if (i != -1) {
          i < 5 && (i = 3);
          new_value = new_value.slice(0, i);
        }

        var reg = matrix.substr(0, this.value.length).replace(/_+/g, function (a) {
          return "\\d{1," + a.length + "}";
        }).replace(/[+()]/g, "\\$&");
        reg = new RegExp("^" + reg + "$");
        if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) this.value = new_value;
        if (event.type == "blur" && this.value.length < 5) this.value = "";
      }

      input.addEventListener("input", mask, false);
      input.addEventListener("focus", mask, false);
      input.addEventListener("blur", mask, false);
      input.addEventListener("keydown", mask, false);
    });
  }); //вывод имени файла

  $(document).ready(function () {
    $('input[type="file"]').change(function (e) {
      var fileName = e.target.files[0].name;
      $('#filename').html(fileName);
    });
  }); //окно благодарности

  var blockThanks = document.getElementById('thanks');
  var blockExpectation = document.getElementById('expectation');
  blockThanks.addEventListener('click', function () {
    blockThanks.style.display = 'none';
  }); //валидация

  var form = document.getElementById("application__form");
  $("#application__form").validate({
    ignore: "#fileInput",
    rules: {
      name: {
        required: true
      },
      phone: {
        required: true,
        minlength: 16
      },
      email: {
        required: true,
        email: true
      },
      application_check: {
        required: true
      }
    },
    messages: {
      name: {
        required: 'Поле обязательно для заполнения'
      },
      phone: {
        required: 'Поле обязательно для заполнения',
        minlength: 'Введите номер в формате "+7 (123) 456-78-90"'
      },
      email: {
        required: 'Поле обязательно для заполнения',
        email: 'Введите корректный email'
      },
      application_check: {
        required: 'Поле обязательно для заполнения'
      }
    },
    submitHandler: function submitHandler() {
      blockExpectation.style.display = 'flex';
      var formData = new FormData(form);
      fetch("../../send.php", {
        method: "POST",
        body: formData
      }).then(function (response) {
        return response.text();
      }).then(function (data) {
        blockExpectation.style.display = 'none';
        blockThanks.style.display = 'flex';
        $('#application__form')[0].reset();
        $('#filename').html('Прикрепить файл с информацией о проекте');
      })["catch"](function (error) {
        return console.error(error);
      });
    }
  });
  form.addEventListener("submit", function (event) {
    event.preventDefault();
  });
  $("#application__form").on("keyup change", function () {
    if ($(this).valid()) {
      $("button[type='submit']").prop("disabled", false);
    } else {
      $("button[type='submit']").prop("disabled", true);
    }
  });
});

/***/ }),

/***/ "./src/sass/main.scss":
/*!****************************!*\
  !*** ./src/sass/main.scss ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/app": 0,
/******/ 			"css/main": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkgazewallet"] = self["webpackChunkgazewallet"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/main"], () => (__webpack_require__("./src/js/app.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/main"], () => (__webpack_require__("./src/sass/main.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;